1.Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
    var - это старый способ инициализации переменной в JS, он схож с let. Основная разница между ними заключается в областях видимости, у var она ограничивается функцией, а у let - блоком. Const это почти тоже что и let, только не позволяет изменять значение.
2.Почему объявлять переменную через var считается плохим тоном?
    Во-первых, потому что это противоречит новой документации (ES6), а во-вторых, потому что это неудобно, что переменная лезет туда, где её не должно существовать. Полагаю что так.
    